#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

const int ignatov_id = 7;

void processClientNumber(int clientNumber, char *buffer);
char *itoa(int v, char *s, int rad);

int main(int argc, char *argv[])
{

    printf("Server started\n");

    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;
    char *buf = calloc(1024, sizeof(char));

    printf("socket()\n");

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listenfd < 0)
    {
        printf("\n socket error occured\n");
        return 1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    printf("socket() done");

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(5000);

    printf("bind() \n");
    bind(listenfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    if (bind < 0)
    {
        return -1;
    }
    printf("bind() done \n");

    if (listen(listenfd, 10) < 0)
    {
        return -1;
    }
    printf("listen done()\n");

    int numberFromClient, tmp;

    while (1)
    {
        printf("wait for accept()\n");
        connfd = accept(listenfd, (struct sockaddr *)NULL, NULL);
        printf("accept() done\n");
        read(connfd, &tmp, sizeof(&tmp));
        numberFromClient = ntohl(tmp);
        processClientNumber(numberFromClient, buf);
        close(connfd);
        sleep(1);
    }
}

void processClientNumber(int clientNumber, char *buffer)
{
    printf("Number in numberal system 10: %d\n", clientNumber);
    printf("Number in numberal system 2: %s\n", itoa(clientNumber, buffer, 2));
    printf("Number in numberal system 16: %s\n", itoa(clientNumber, buffer, 16));
    printf("Number in numberal system 8: %s\n", itoa(clientNumber, buffer, 8));
    printf("Number in numberal system %d: %s\n", ignatov_id, itoa(clientNumber, buffer, ignatov_id));
}

char *itoa(int v, char *s, int rad)
{
    int k = 0;
    while (v)
    {
        int x = v % rad;
        s[k++] = (x < 10) ? '0' + x : 'A' + x - 10;
        v /= rad;
    }
    s[k] = 0;
    for (int i = 0; i < k / 2; i++)
    {
        int t = s[i];
        s[i] = s[k - i - 1];
        s[k - i - 1] = t;
    }
    return s;
}
